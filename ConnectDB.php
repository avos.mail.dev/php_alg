<?php
require_once 'autoload.php';

// $conf = Config::getInstance();
// $conf = Config::hello();

// $dbConfig = Config::getInstance()->get('db');
// $db = new PDO(
//     'mysql:host=127.0.0.1;dbname=alg3',
//     $dbConfig['user'],
//     $dbConfig['password']
// );

// $sql = 'SELECT * FROM birds';
// foreach ($db->query($sql) as $row) {
//     print_r($row);
// }

// class SomeClass
class ConnectDB
{
    public function __construct()
    {
        $this->dbConfig = Config::getInstance()->get('db');

        $this->db = new PDO(
            'mysql:host=127.0.0.1;dbname=alg3',
            $this->dbConfig['user'],
            $this->dbConfig['password']
        );
    }

    public function getAll()
    {
        $sql = 'SELECT * FROM birds';
        foreach ($this->db->query($sql) as $row) {
            print_r($row);
        }
    }
}

// $cls = new ConnectDB();
// $cls->getAll();

?>
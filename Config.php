<?php
class Config {
    private $data;
    private static $instance;

    private function __construct()
    {
        $json = file_get_contents('app.json');
        $this->data = json_decode($json, true);
    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new Config();
        }
        return self::$instance;
    }

    public static function hello()
    {
        echo "Hello from Singleton.";
    }

    public function get($key)
    {
        try {
            return $this->data[$key];
        } catch (Exception $e) {
            echo 'Error getting: ' .$key;
        }
    }
}
?>